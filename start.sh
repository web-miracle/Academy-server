#!/bin/bash

read_var() {
    VAR=$(grep $1 $2 | xargs)
    IFS="=" read -ra VAR <<< "$VAR"
    echo ${VAR[1]}
}

BACK_IMAGE=$(read_var BACK_IMAGE .env)

docker login registry.gitlab.com/yugin/academy
docker pull $BACK_IMAGE

docker login registry.gitlab.com/web-miracle/mentors-admin
docker pull registry.gitlab.com/web-miracle/mentors-admin:develop

docker system prune -f
docker-compose -f $(pwd)/docker-compose.yml up -d
